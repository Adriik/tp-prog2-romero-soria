package main;

public class Mayor65 extends Mesa {

	final static Integer horaMaxima = 17;
	final static Integer cupoPorHora = 10;
	
	public Mayor65(Votante presidente, int id) {
		super(presidente, id, horaMaxima, cupoPorHora);
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (getClass() != obj.getClass())
			return false;
		if (!super.equals(obj))
			return false;
		return true;
	}
}
