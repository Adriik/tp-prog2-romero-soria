package main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class Mesa {
	private FranjaHoraria _franjaHorariaActual;
	private final Integer _horarioMaximo;
	private final Integer _cupo;
	private final Integer _id;
	private final Votante _presidenteMesa;
	private Set<Votante> _votantesAsignados;
	private Map<Integer, List<Integer>> _votantesPorHora;
	final static Integer horaMinima = 8;

	public Mesa(Votante presidente, int id, int horarioMaximo, int cupo) {
		if (id <= 0)
			throw new RuntimeException("ID inválido.");

		_horarioMaximo = horarioMaximo;
		_cupo = cupo;
		_presidenteMesa = presidente;
		_id = id;
		_votantesAsignados = new HashSet<>();
		_franjaHorariaActual = new FranjaHoraria(horaMinima, cupo);
		_votantesPorHora = new HashMap<>();
		_votantesPorHora.put(horaMinima, new ArrayList<Integer>());
	}

	public Integer asignarHorario(Votante votante) {
		Integer horario = null;
		_franjaHorariaActual.disminuirEspacioDisponible();
		_votantesPorHora.get(_franjaHorariaActual.getHora()).add(votante.getDni());
		horario = _franjaHorariaActual.getHora();
		_votantesAsignados.add(votante);
		if (_franjaHorariaActual.getHora() < _horarioMaximo && _franjaHorariaActual.espacioDisponible() == 0) {
			_franjaHorariaActual = new FranjaHoraria(_franjaHorariaActual.getHora() + 1, _cupo);
			_votantesPorHora.put(_franjaHorariaActual.getHora(), new ArrayList<Integer>());
		}
		return horario;
	}

	public int cantidadVotantesConTurno() {
		return _votantesAsignados.size();
	}

	public boolean hayLugar() {
		return (_franjaHorariaActual.getHora() < _horarioMaximo
				|| (_franjaHorariaActual.getHora().equals(_horarioMaximo)
						&& _franjaHorariaActual.espacioDisponible() > 0));
	}

	public Map<Integer, List<Integer>> getVotantesAsignados() {
		return _votantesPorHora;
	}

	public Integer getId() {
		return _id;
	}

	@Override
	public String toString() {
		final StringBuilder str = new StringBuilder();
		str.append("Numero de mesa: " + _id + ". Tipo de mesa: " + this.getClass().getSimpleName() + ". Presidente: "
				+ _presidenteMesa.getNombre());
		return str.toString();
	}

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_id == null) ? 0 : _id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mesa other = (Mesa) obj;
		if (!_id.equals(other._id))
			return false;
		return true;
	}

}
