package main;

public class Turno {

	private final Mesa _mesa;
	private final Integer _horario;

	public Turno(Mesa mesa, Integer horario) {
		if (mesa == null || horario == null)
			throw new RuntimeException("Mesa y/o horario inválido.");
		_mesa = mesa;
		_horario = horario;
	}

	public Mesa getMesa() {
		return _mesa;
	}

	public Integer getHorario() {
		return _horario;
	}

	public Tupla<Integer, Integer> getTurnoTupla() {
		return new Tupla<Integer, Integer>(_mesa.getId(), _horario);
	}
	
	@Override
	public String toString() {
		final StringBuilder str = new StringBuilder();
		str.append("Mesa: " + _mesa.getId() + ", Horario: " + _horario + " hs. ");

		return str.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_horario == null) ? 0 : _horario.hashCode());
		result = prime * result + ((_mesa == null) ? 0 : _mesa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Turno other = (Turno) obj;
		if (!_horario.equals(other._horario))
			return false;
		if (!_mesa.equals(other._mesa))
			return false;
		return true;
	}

}
