package main;

public class PresidenteMesa extends Votante {
	private final Mesa _mesa;

	public PresidenteMesa(int dni, Mesa mesa) {
		super(dni);
		_mesa = mesa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((_mesa == null) ? 0 : _mesa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (getClass() != obj.getClass())
			return false;
		if (!super.equals(obj))
			return false;
		PresidenteMesa other = (PresidenteMesa) obj;
		if (_mesa == null) {
			if (other._mesa != null)
				return false;
		} else if (!_mesa.equals(other._mesa))
			return false;
		return true;
	}
	
}
