package main;

public class General extends Mesa {

	final static Integer horaMaxima = 17;
	final static Integer cupoPorHora = 30;
	
	public General(Votante presidente, int id) {
		super(presidente, id, horaMaxima, cupoPorHora);
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (getClass() != obj.getClass())
			return false;
		if (!super.equals(obj))
			return false;
		return true;
	}

}
