package main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SistemaDeTurnos {
	private Set<Mesa> _mesas;
	private Set<Votante> _votantesRegistrados;
	private Set<PresidenteMesa> _presidentes;
	private Map<Integer, Turno> _turnosPorDni;
	private String _nombreSistema;
	private Integer _numeroMesa;

	public SistemaDeTurnos(String nombreSistema) {
		_mesas = new HashSet<>();
		_presidentes = new HashSet<>();
		_turnosPorDni = new HashMap<>();
		_votantesRegistrados = new HashSet<>();
		_numeroMesa = 0;

		if (nombreSistema != null)
			_nombreSistema = nombreSistema;
		else
			throw new RuntimeException("Nombre de sistema inválido.");
	}

	public void registrarVotante(int dni, String nombre, int edad, boolean enfPrevia, boolean trabaja) {
		Votante votante = new Votante(dni, nombre, edad, enfPrevia, trabaja);
		if (_votantesRegistrados.contains(votante)) {
			throw new RuntimeException("El votante ya está registrado.");
		} else {
			_votantesRegistrados.add(votante);
			_turnosPorDni.put(dni, null);
		}
	}

	public int agregarMesa(final String tipoMesa, int dni) {
		Votante votante = verificarQueEsteRegistrado(dni);
		if (_presidentes.contains(votante))
			throw new RuntimeException("El votante ya es presidente de otra mesa.");
		else if (_turnosPorDni.get(dni) != null)
			throw new RuntimeException("El votante ya tiene un turno asignado.");

		_numeroMesa++;
		int id = _numeroMesa;
		Mesa nuevaMesa = null;
		switch (tipoMesa) {
		case "Mayor65":
			nuevaMesa = new Mayor65(votante, id);
			break;
		case "General":
			nuevaMesa = new General(votante, id);
			break;
		case "Trabajador":
			nuevaMesa = new Trabajador(votante, id);
			break;
		case "Enf_Preex":
			nuevaMesa = new Enf_Preex(votante, id);
			break;
		default:
			throw new RuntimeException("Tipo de mesa inválido.");
		}
		_mesas.add(nuevaMesa);
		asignarTurnoPresidente(votante, nuevaMesa);
		votante = new PresidenteMesa(votante.getDni().intValue(), nuevaMesa);
		_presidentes.add((PresidenteMesa) votante);
		return id;
	}

	private void asignarTurnoPresidente(Votante votante, Mesa mesa) {
		Integer hora = mesa.asignarHorario(votante);
		votante.asignarTurno(mesa, hora);
		_turnosPorDni.put(votante.getDni(), new Turno(mesa, hora));
	}

	public Tupla<Integer, Integer> asignarTurno(int dni) {
		Votante votante = verificarQueEsteRegistrado(dni);
		if (_turnosPorDni.get(dni) != null) {
			return _turnosPorDni.get(dni).getTurnoTupla();
		} else {
			Mesa mesa = asignarMesa(votante);
			if (mesa == null)
				return null;
			Integer hora = mesa.asignarHorario(votante);
			votante.asignarTurno(mesa, hora);
			_turnosPorDni.put(votante.getDni(), new Turno(mesa, hora));
			return _turnosPorDni.get(dni).getTurnoTupla();
		}
	}

	public int asignarTurnos() {
		int turnosAsignados = 0;
		for (Votante v : _votantesRegistrados) {
			if (v.getTurno() == null) {
				Mesa mesa = asignarMesa(v);
				if (mesa != null) {
					Integer hora = mesa.asignarHorario(v);
					v.asignarTurno(mesa, hora);
					_turnosPorDni.put(v.getDni(), new Turno(mesa, hora));
					++turnosAsignados;
				}
			}
		}
		return turnosAsignados;
	}

	private Mesa asignarMesa(Votante votante) {
		String tipoDeMesa = debeIrAMesa(votante);
		Mesa mesa = null;
		for (Mesa m : _mesas) {
			if (m.getClass().getSimpleName().equals(tipoDeMesa) && m.hayLugar())
				mesa = m;
		}
		if (mesa == null && tipoDeMesa.equals("Enf_Preex") && votante.edad() >= 65) {
			for (Mesa m : _mesas) {
				if (m.getClass().getSimpleName().equals("Mayor65") && m.hayLugar())
					mesa = m;
			}
		}
		return mesa;
	}

	private String debeIrAMesa(Votante votante) {
		if (votante.debeTrabajar())
			return "Trabajador";
		else {
			if (votante.padeceEnfermedad()) {
				return "Enf_Preex";
			}
			if (votante.edad() >= 65) {
				return "Mayor65";
			}
		}
		return "General";
	}

	private Votante verificarQueEsteRegistrado(int dni) {
		for (Votante v : _votantesRegistrados) {
			if (v.getDni().intValue() == dni)
				return v;
		}
		throw new RuntimeException("El votante no está registrado.");
	}

	public boolean votar(int dni) {
		Votante votante = verificarQueEsteRegistrado(dni);
		if (votante.yaVoto())
			return false;
		votante.irAVotar();
		return true;
	}

	public Map<Integer, List<Integer>> asignadosAMesa(int numMesa) {
		for (Mesa m : _mesas) {
			if (m.getId().intValue() == numMesa)
				return m.getVotantesAsignados();
		}
		throw new RuntimeException("Número de mesa inválido.");
	}

	public int votantesConTurno(final String tipoMesa) {
		int votantesConTurno = 0;
		if (!tipoMesa.equals("Mayor65") && !tipoMesa.equals("General") && !tipoMesa.equals("Trabajador")
				&& !tipoMesa.equals("Enf_Preex"))
			throw new RuntimeException("Tipo de mesa inválido.");
		for (Mesa m : _mesas) {
			if (m.getClass().getSimpleName().equals(tipoMesa))
				votantesConTurno += m.cantidadVotantesConTurno();
		}
		return votantesConTurno;
	}

	public Tupla<Integer, Integer> consultaTurno(int dni) {
		if (!_turnosPorDni.containsKey(dni))
			throw new RuntimeException("El votante no esta registrado");
		else if (_turnosPorDni.get(dni) == null)
			return null;
		return _turnosPorDni.get(dni).getTurnoTupla();
	}

	public List<Tupla<String, Integer>> sinTurnoSegunTipoMesa() {
		List<Tupla<String, Integer>> sinTurno = new ArrayList<Tupla<String, Integer>>();
		Integer mesasMayores = 0;
		Integer mesasTrabajadores = 0;
		Integer mesasEnfermedad = 0;
		Integer mesasGeneral = 0;

		for (Votante v : _votantesRegistrados) {
			if (_turnosPorDni.get(v.getDni()) == null) {
				String mesa = debeIrAMesa(v);
				if (mesa.equals("Mayor65")) {
					mesasMayores++;
				} else if (mesa.equals("General")) {
					mesasGeneral++;
				} else if (mesa.equals("Trabajador")) {
					mesasTrabajadores++;
				} else if (mesa.equals("Enf_Preex")) {
					mesasEnfermedad++;
				}
			}
		}
		sinTurno.add(new Tupla<>("Mayor65", mesasMayores));
		sinTurno.add(new Tupla<>("General", mesasGeneral));
		sinTurno.add(new Tupla<>("Trabajador", mesasTrabajadores));
		sinTurno.add(new Tupla<>("Enf_Preex", mesasEnfermedad));
		return sinTurno;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Sistema de Turnos para Votación: " + _nombreSistema + "\n");
		final Iterator<Votante> itrVotanteTurno = _votantesRegistrados.iterator();
		final Iterator<Votante> itrVotanteSinTurno = _votantesRegistrados.iterator();
		final Iterator<Mesa> itrMesas = _mesas.iterator();
		Votante votanteActual;
		Mesa mesaActual;
		sb.append("Votantes sin turno:\n");
		while (itrVotanteSinTurno.hasNext()) {
			votanteActual = (Votante) itrVotanteSinTurno.next();
			if (votanteActual.getTurno() == null) {
				sb.append("\t" + votanteActual.toString() + "\n");
			}
		}
		sb.append("Votantes con turno:\n");
		while (itrVotanteTurno.hasNext()) {
			votanteActual = (Votante) itrVotanteTurno.next();
			if (votanteActual.getTurno() != null) {
				sb.append("\t" + votanteActual.toString() + "\n");
			}
		}
		sb.append("Mesas:\n");
		while (itrMesas.hasNext()) {
			mesaActual = (Mesa) itrMesas.next();
			sb.append("\t" + mesaActual.toString() + "\n");
		}
		return sb.toString();
	}

}
