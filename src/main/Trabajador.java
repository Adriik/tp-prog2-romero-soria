package main;

public class Trabajador extends Mesa {

	final static Integer horaMaxima = 11;
	final static Integer cupoPorHora = 30;
	
	public Trabajador(Votante presidente, int id) {
		super(presidente, id, horaMaxima, cupoPorHora);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (getClass() != obj.getClass())
			return false;
		if (!super.equals(obj))
			return false;
		return true;
	}	

}
