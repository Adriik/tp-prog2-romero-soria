package main;

import java.util.Objects;

public class Votante {
	private String _nombre;
	private Integer _dni;
	private Integer _edad;
	private boolean _padeceEnfermedad;
	private boolean _debeTrabajar;
	private boolean _yaVoto;
	private Turno _turno;

	public Votante(int dni, String nombre, int edad, boolean enfPrevia, boolean trabaja) {
		if (dni < 0)
			throw new RuntimeException("El DNI es inválido.");
		if (edad < 16)
			throw new RuntimeException("La persona no tiene edad para votar");
		if (nombre.length() == 0)
			throw new RuntimeException("Nombre inválido");
		_nombre = nombre;
		_dni = dni;
		_edad = edad;
		_padeceEnfermedad = enfPrevia;
		_debeTrabajar = trabaja;
		_yaVoto = false;
	}

	public Votante(int dni) {
		_dni = dni;
	}

	public void asignarTurno(Mesa mesa, Integer hora) {
		_turno = new Turno(mesa, hora);
	}

	public void irAVotar() {
		_yaVoto = true;
	}

	public boolean yaVoto() {
		return _yaVoto;
	}

	public Turno getTurno() {
		return _turno;
	}

	public Integer getDni() {
		return _dni;
	}

	public Integer edad() {
		return _edad;
	}

	public String getNombre() {
		return _nombre;
	}

	public boolean padeceEnfermedad() {
		return _padeceEnfermedad;
	}

	public boolean debeTrabajar() {
		return _debeTrabajar;
	}

	@Override
	public String toString() {
		final StringBuilder str = new StringBuilder();
		str.append("Nombre: " + _nombre + ". ");
		str.append("DNI: " + _dni + ". ");
		if (_turno != null) {
			str.append("Turno: " + _turno.toString());
			str.append(_yaVoto == true ? "Ya votó. " : "Aún no votó. ");
		} else {
			str.append(_turno != null ? _turno.toString() : "Sin turno. ");
		}
		return str.toString();
	}

	@Override
	public int hashCode() {
		return Objects.hash(_dni);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Votante other = (Votante) obj;
		return Objects.equals(_dni, other._dni);
	}

}
