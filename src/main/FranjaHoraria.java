package main;

public class FranjaHoraria {
	private final Integer _hora;
	private Integer _cupo;
	
	public FranjaHoraria(Integer hora, Integer cupo) {
		if (hora < 0 || hora == null || cupo <= 0 || cupo == null)
			throw new RuntimeException("Hora o cupo invalido/s" + hora + " " + cupo);
		_hora = hora;
		_cupo = cupo;
	}
	
	public Integer espacioDisponible() {
		return _cupo;
	}
	
	public void disminuirEspacioDisponible() {
		_cupo--;
	}

	public Integer getHora() {
		return _hora;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_cupo == null) ? 0 : _cupo.hashCode());
		result = prime * result + ((_hora == null) ? 0 : _hora.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FranjaHoraria other = (FranjaHoraria) obj;
		if (!_cupo.equals(other._cupo))
			return false;
		if (!_hora.equals(other._hora))
			return false;
		return true;
	}

}
