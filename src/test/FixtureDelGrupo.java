package test;

public class FixtureDelGrupo {

	public static final FixtureDelGrupo INSTANCE = new FixtureDelGrupo();

	final Boolean trabaja = true;
	final Boolean tieneEnfPrevia = true;

	final String mesaEnfermos = "Enf_Preex";
	final String mesaMayores = "Mayor65";
	final String mesaGeneral = "General";
	final String mesaTrabajadores = "Trabajador";
	final String mesaInvalida = "Cafeteria";

	final Integer dniAdrian = 42045453;
	final Integer dniLuci = 43911246;
	final Integer dniAgustin = 42033987;
	final Integer dniRomero = 42045123;
	final Integer dniCarla = 45342088;
	final Integer dniMario = 11928347;
	final Integer dniRoberto = 12345678;
	final Integer dniTulio = 87564321;
	final Integer dniElPepe = 13578642;
	final Integer dniLuigi = 13578620;

	final Integer cupoXFranjaHorariaEnfPreexistente = 20;
	final Integer cantDnis = 200;
	final Integer edad = 70;

}