package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import main.FranjaHoraria;
import main.General;
import main.Mayor65;
import main.PresidenteMesa;
import main.SistemaDeTurnos;
import main.Tupla;
import main.Turno;
import main.Votante;

public class SistemaTurnosTestDelGrupo {
	private SistemaDeTurnos sistemaTurnos;
	private static final FixtureDelGrupo F = FixtureDelGrupo.INSTANCE;

	@Before
	public void setUp() {
		sistemaTurnos = new SistemaDeTurnos("Sistema de votación");

		// Registrar votantes
		// Presidentes
		sistemaTurnos.registrarVotante(F.dniAdrian, "Adrián", 22, !F.tieneEnfPrevia, !F.trabaja); // General
		sistemaTurnos.registrarVotante(F.dniAgustin, "Agustín", 18, !F.tieneEnfPrevia, F.trabaja); // Trabajador
		sistemaTurnos.registrarVotante(F.dniRomero, "Romero", 70, !F.tieneEnfPrevia, !F.trabaja); // Mayor
		sistemaTurnos.registrarVotante(F.dniCarla, "Carla", 33, F.tieneEnfPrevia, !F.trabaja); // Enfermedad

		// Votantes
		sistemaTurnos.registrarVotante(F.dniMario, "Mario", 46, !F.tieneEnfPrevia, !F.trabaja); // General
		sistemaTurnos.registrarVotante(F.dniLuci, "Lucía", 19, !F.tieneEnfPrevia, F.trabaja); // Trabaja
		sistemaTurnos.registrarVotante(F.dniRoberto, "Roberto", 89, !F.tieneEnfPrevia, !F.trabaja); // Mayor
		sistemaTurnos.registrarVotante(F.dniTulio, "Tulio", 22, F.tieneEnfPrevia, !F.trabaja); // Enfermedad
		sistemaTurnos.registrarVotante(F.dniElPepe, "El pepe", 33, F.tieneEnfPrevia, F.trabaja); // Trabaja +
																									// Enfermedad
		sistemaTurnos.registrarVotante(F.dniLuigi, "Luigi", 127, F.tieneEnfPrevia, F.trabaja); // Trabaja + Enfermedad
																								// + Mayor
	}

	@Test
	public void obtenerTurnosTest() {
		sistemaTurnos.agregarMesa(F.mesaTrabajadores, F.dniAgustin);
		sistemaTurnos.agregarMesa(F.mesaMayores, F.dniRomero);
		sistemaTurnos.asignarTurno(F.dniLuci);

		Tupla<Integer, Integer> turnoEsperado = new Tupla<>(1, 8);
		Tupla<Integer, Integer> turnoPresidente = new Tupla<>(2, 8);

		assertEquals(turnoEsperado, sistemaTurnos.consultaTurno(F.dniLuci));
		assertEquals(turnoPresidente, sistemaTurnos.consultaTurno(F.dniRomero));
		assertEquals(null, sistemaTurnos.consultaTurno(F.dniTulio));
	}

	@Test
	public void presidentesTest() {
		final int idEsperado = 1;
		assertEquals(idEsperado, sistemaTurnos.agregarMesa(F.mesaTrabajadores, F.dniAgustin));
		try {
			sistemaTurnos.agregarMesa(F.mesaMayores, F.dniAgustin);
			assertTrue(false);
		} catch (Exception e) {
			assertNotNull(e);
		}
	}

	@Test
	public void toStringTest() {
		// Presidentes
		sistemaTurnos.agregarMesa(F.mesaGeneral, F.dniAdrian);
		sistemaTurnos.agregarMesa(F.mesaMayores, F.dniRomero);
		sistemaTurnos.agregarMesa(F.mesaTrabajadores, F.dniAgustin);
		sistemaTurnos.agregarMesa(F.mesaEnfermos, F.dniCarla);

		System.out.println("Primer Estado:");
		System.out.println(sistemaTurnos);

		// Votantes
		sistemaTurnos.asignarTurno(F.dniMario);
		sistemaTurnos.asignarTurno(F.dniLuci);
		sistemaTurnos.asignarTurnos();

		System.out.println("Segundo estado:");
		System.out.println(sistemaTurnos);

		// Votar
		sistemaTurnos.votar(F.dniMario);
		sistemaTurnos.votar(F.dniLuci);
		sistemaTurnos.votar(F.dniRoberto);
		sistemaTurnos.votar(F.dniTulio);
		sistemaTurnos.votar(F.dniElPepe);
		sistemaTurnos.votar(F.dniLuigi);

		System.out.println("Tercer estado:");
		System.out.println(sistemaTurnos);
	}

	@Test
	public void equalsTest() {
		Votante persona0 = new Votante(43911246, "Lucia", 19, false, false);
		Votante persona1 = new Votante(43911246, "Adrian", 22, false, false);
		Votante persona2 = new Votante(43911247, "Adrian", 22, false, false);

		General mesa1 = new General(persona0, 123);
		General mesa2 = new General(persona0, 123);
		General mesa3 = new General(persona0, 124);
		Mayor65 mesa4 = new Mayor65(persona0, 124);

		// Mesas
		assertTrue(mesa1.equals(mesa2));
		assertFalse(mesa1.equals(mesa3));
		assertFalse(mesa3.equals(mesa4));

		// Votantes
		assertTrue(persona0.equals(persona1));
		assertFalse(persona1.equals(persona2));

		// Franjas horarias
		FranjaHoraria franja1 = new FranjaHoraria(8, 5);
		FranjaHoraria franja2 = new FranjaHoraria(8, 5);
		FranjaHoraria franja3 = new FranjaHoraria(9, 5);
		assertTrue(franja1.equals(franja2));
		assertFalse(franja2.equals(franja3));
		
		try {
			FranjaHoraria franjaMalvada = new FranjaHoraria(null, null);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// Presidentes de mesa
		PresidenteMesa presidente1 = new PresidenteMesa(43911248, mesa1);
		PresidenteMesa presidente2 = new PresidenteMesa(43911248, mesa1);
		PresidenteMesa presidente3 = new PresidenteMesa(43911249, mesa1);
		PresidenteMesa presidente4 = new PresidenteMesa(43911248, mesa3);
		assertTrue(presidente1.equals(presidente2));
		assertFalse(presidente2.equals(presidente3));
		assertFalse(presidente2.equals(presidente4));
		// Turnos
		Turno t1 = new Turno(mesa1, 8);
		Turno t2 = new Turno(mesa1, 8);
		Turno t3 = new Turno(mesa1, 9);

		assertTrue(t1.equals(t2));
		assertFalse(t2.equals(t3));
	}

	@Test
	public void asignadosAMesaTest() {
		int mesa1 = sistemaTurnos.agregarMesa(F.mesaGeneral, F.dniAdrian);
		int mesa2 = sistemaTurnos.agregarMesa(F.mesaEnfermos, F.dniLuci);
		sistemaTurnos.asignarTurno(F.dniMario);

		Map<Integer, List<Integer>> mesa1Esperado = new HashMap<>();
		Map<Integer, List<Integer>> mesa2Esperado = new HashMap<>();

		mesa1Esperado.put(8, new ArrayList<Integer>());
		mesa1Esperado.get(8).add(F.dniAdrian);
		mesa1Esperado.get(8).add(F.dniMario);

		mesa2Esperado.put(8, new ArrayList<Integer>());
		mesa2Esperado.get(8).add(F.dniLuci);

		assertEquals(mesa1Esperado, sistemaTurnos.asignadosAMesa(mesa1));
		assertEquals(mesa2Esperado, sistemaTurnos.asignadosAMesa(mesa2));
	}

	@Test
	public void mesaMayoresYEnfermosTest() {
		final List<Integer> dnis = generarNDnis(F.cantDnis);
		final Integer dniMayorYEnfermo = 888;

		sistemaTurnos.agregarMesa(F.mesaEnfermos, F.dniAdrian);
		final Integer numMesaMayores = sistemaTurnos.agregarMesa(F.mesaMayores, F.dniCarla);
		for (Integer documento : dnis) {
			sistemaTurnos.registrarVotante(documento, documento.toString(), 70, true, false);
		}
		sistemaTurnos.registrarVotante(dniMayorYEnfermo, dniMayorYEnfermo.toString(), 70, true, false);
		sistemaTurnos.asignarTurnos();

		assertEquals(numMesaMayores, sistemaTurnos.consultaTurno(dniMayorYEnfermo).getX());
	}

	private List<Integer> generarNDnis(Integer n) {
		List<Integer> dnis = new ArrayList<Integer>();
		for (int i = 1; i < F.cantDnis; i++) {
			dnis.add(i);
		}
		return dnis;
	}

	@Test
	public void votantesConTurnoTest() {
		sistemaTurnos.agregarMesa("General", F.dniAdrian);
		sistemaTurnos.asignarTurnos();
		final int cantTurnos = sistemaTurnos.votantesConTurno("General");

		// Solo 2 de los votantes son generales, por lo que tienen turno.

		assertEquals(cantTurnos, 2);
	}

	@Test
	public void cuposLlenosTest() {
		SistemaDeTurnos sistemaTurnos2 = new SistemaDeTurnos("Sistema de votación");

		final int cupoMaximoGeneral = 300;
		final int cupoMaximoMayores = 100;
		final int cupoMaximoEnfermedad = 200;
		final int cupoMaximoTrabajadores = 120;
		final int cuposMaximos = cupoMaximoEnfermedad + cupoMaximoGeneral + cupoMaximoMayores + cupoMaximoTrabajadores
				+ 100;

		for (int i = 1; i <= cuposMaximos; i++) {
			if (i <= cupoMaximoGeneral + 25)
				sistemaTurnos2.registrarVotante(i, "Votante" + i, 20, false, false);
			else if (i <= cupoMaximoGeneral + cupoMaximoEnfermedad + 50)
				sistemaTurnos2.registrarVotante(i, "Votante" + i, 20, F.tieneEnfPrevia, false);
			else if (i <= cupoMaximoGeneral + cupoMaximoEnfermedad + cupoMaximoMayores + 75)
				sistemaTurnos2.registrarVotante(i, "Votante" + i, 90, false, false);
			else if (i <= cuposMaximos)
				sistemaTurnos2.registrarVotante(i, "Votante" + i, 30, false, F.trabaja);
		}

		sistemaTurnos2.agregarMesa(F.mesaGeneral, 1);
		sistemaTurnos2.agregarMesa(F.mesaEnfermos, cupoMaximoGeneral + 25 + 1);
		sistemaTurnos2.agregarMesa(F.mesaMayores, cupoMaximoGeneral + cupoMaximoEnfermedad + 50 + 1);
		sistemaTurnos2.agregarMesa(F.mesaTrabajadores,
				cupoMaximoGeneral + cupoMaximoEnfermedad + cupoMaximoMayores + 75 + 1);

		final int turnosAsignados = sistemaTurnos2.asignarTurnos() + 4; // Los presidentes no son contados ya que
																			// tienen turno
		final List<Tupla<String, Integer>> votantesSinTurnoTotal = sistemaTurnos2.sinTurnoSegunTipoMesa();

		final int votantesConTurnoGeneral = sistemaTurnos2.votantesConTurno("General");
		final int votantesConTurnoMayores = sistemaTurnos2.votantesConTurno("Mayor65");
		final int votantesConTurnoEnfermedad = sistemaTurnos2.votantesConTurno("Enf_Preex");
		final int votantesConTurnoTrabajadores = sistemaTurnos2.votantesConTurno("Trabajador");

		assertEquals(votantesConTurnoGeneral, cupoMaximoGeneral);
		assertEquals(votantesConTurnoEnfermedad, cupoMaximoEnfermedad);
		assertEquals(votantesConTurnoMayores, cupoMaximoMayores);
		assertEquals(votantesConTurnoTrabajadores, cupoMaximoTrabajadores);

		assertEquals(turnosAsignados, votantesConTurnoEnfermedad + votantesConTurnoGeneral + votantesConTurnoMayores
				+ votantesConTurnoTrabajadores);

		assertEquals(votantesSinTurnoTotal.get(0).getY().intValue(), 25);
		assertEquals(votantesSinTurnoTotal.get(1).getY().intValue(), 25);
		assertEquals(votantesSinTurnoTotal.get(2).getY().intValue(), 25);
		assertEquals(votantesSinTurnoTotal.get(3).getY().intValue(), 25);

	}
}
